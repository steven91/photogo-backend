package com.certimeter.photoGo.repository;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.certimeter.photoGo.mapper.ScanMapper;
import com.certimeter.photoGo.model.Scan;
import com.certimeter.photoGo.model.ScanDetail;

@Repository
public class ScanRepository {

	@Autowired
	ScanMapper scanMapper;
	
	public ArrayList<Scan> getScans() { return scanMapper.getScans(); }
	
	public int insertScan(Scan scan) { return scanMapper.insertScan(scan); }
	
	public void insertScanHistory(ScanDetail scanDetail) { scanMapper.insertScanHistory(scanDetail); }
}
