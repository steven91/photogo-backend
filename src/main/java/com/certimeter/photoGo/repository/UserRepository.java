package com.certimeter.photoGo.repository;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.certimeter.photoGo.mapper.UserMapper;
import com.certimeter.photoGo.model.User;
import com.certimeter.photoGo.request.LoginRequest;
import com.certimeter.photoGo.request.SinginRequest;

@Repository
public class UserRepository {

	@Autowired
	UserMapper userMapper;
	
	public User getUser(int idUser) { return userMapper.getUser(idUser); }
	
	public User login(LoginRequest loginRequest) { return userMapper.login(loginRequest); }
	
	public int singin(SinginRequest singinRequest) { return userMapper.singin(singinRequest); }
	
	public ArrayList<User> rank() { return userMapper.rank(); }
	
	public ArrayList<User> checkExist(SinginRequest singinRequest) { return userMapper.checkExist(singinRequest); }
	
	public int updateUserScore(User user) { return userMapper.updateUserScore(user); }
	
}
