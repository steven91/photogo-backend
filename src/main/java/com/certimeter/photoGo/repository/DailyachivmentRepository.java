package com.certimeter.photoGo.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.certimeter.photoGo.mapper.DailyachivmentMapper;
import com.certimeter.photoGo.model.DailyAchivment;

@Repository
public class DailyachivmentRepository {

	@Autowired
	DailyachivmentMapper dailyachivmentMapper;
	
	public DailyAchivment getAchivment(int day) { return dailyachivmentMapper.getAchivment(day); }
}
