package com.certimeter.photoGo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.certimeter.photoGo.model.DailyAchivment;
import com.certimeter.photoGo.repository.DailyachivmentRepository;

@Service
public class DailyachivmentService {

	@Autowired
	DailyachivmentRepository dailyachivmentRepository;
	
	public DailyAchivment getAchivment(int day) { return dailyachivmentRepository.getAchivment(day); }
	
}
