package com.certimeter.photoGo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.certimeter.photoGo.model.User;
import com.certimeter.photoGo.repository.UserRepository;
import com.certimeter.photoGo.request.LoginRequest;
import com.certimeter.photoGo.request.SinginRequest;
import com.certimeter.photoGo.utility.UserUtility;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	public User getUser(int idUser) { return userRepository.getUser(idUser); }
	
	public User login(LoginRequest loginRequest) { return userRepository.login(loginRequest); }
	
	public int singin(SinginRequest singinRequest) { return userRepository.singin(singinRequest); }
	
	public ArrayList<User> rank() { return userRepository.rank(); }
	
	public int updateScore(double confidence, int idUser) {
		User user = getUser(idUser);
		UserUtility userUtility = new UserUtility();
		int point = userUtility.calculatePoint(confidence);
		if(point > 0) {
			user.setScore(user.getScore() + point);
			return userRepository.updateUserScore(user) == 1 ? point : -1;
		}
		return 0; // Caso in cui la confidence sia troppo bassa
	}	
	
	// Funzione di supporto che controlla se l'username o l'email sono già state utilizzate nel db
	public int checkExist(SinginRequest singinRequest) { return userRepository.checkExist(singinRequest).isEmpty() ? 1:0;}
	
}
