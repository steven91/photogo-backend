package com.certimeter.photoGo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.certimeter.photoGo.dto.ObjectDto;
import com.certimeter.photoGo.model.Scan;
import com.certimeter.photoGo.model.ScanDetail;
import com.certimeter.photoGo.repository.ScanRepository;

@Service
public class ScanService {
	
	@Autowired
	ScanRepository scanRepository;

	public ArrayList<Scan> getScans(){ return scanRepository.getScans(); }
	
	public int insertScan(Scan scan) {return scanRepository.insertScan(scan); }
	
	public void insertScanHistory(ArrayList<ObjectDto> foundObjects, Scan targetScan) { 
		for(ObjectDto objectDto: foundObjects) { scanRepository.insertScanHistory(new ScanDetail(0, targetScan.getIdScan(), objectDto.getLabel(), objectDto.getConfidence())); }
	} 
	
}
