package com.certimeter.photoGo.controller;

import java.time.OffsetDateTime;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.certimeter.photoGo.dto.ObjectDto;
import com.certimeter.photoGo.model.DailyAchivment;
import com.certimeter.photoGo.model.Scan;
import com.certimeter.photoGo.response.PostScanResponse;
import com.certimeter.photoGo.service.DailyachivmentService;
import com.certimeter.photoGo.service.ScanService;
import com.certimeter.photoGo.service.UserService;

@RestController
@RequestMapping("scan")
public class ScanController {
	
	@Autowired
	ScanService scanService;
	@Autowired
	UserService userService;
	@Autowired
	DailyachivmentService dailyachivmentService;
	
	@GetMapping("get/all")
	public ResponseEntity<ArrayList<Scan>> getScans(){
		return new ResponseEntity<ArrayList<Scan>>(scanService.getScans(), HttpStatus.OK);
	}

	@PostMapping("post")
	public @ResponseBody ResponseEntity<PostScanResponse> insertScan(@RequestBody ArrayList<ObjectDto> foundObjects, @RequestHeader("ID-USER") int idUser){
		System.out.println("RICHIESTA SCAN " + foundObjects.toString());
		
		if(!foundObjects.isEmpty()) {
			//TODO: Per ora preleviamo un achivment fisso, bisogna modificarlo per prenderlo in base al giorno
			DailyAchivment achivment = dailyachivmentService.getAchivment(1);
			
			for(ObjectDto objectDto: foundObjects) {
				if(objectDto.getLabel().equals(achivment.getTargetObject())) {
					System.out.println("Valore di LocalDateTime : " + OffsetDateTime.now());
					Scan objectScan = new Scan(0, idUser, OffsetDateTime.now(), achivment.getDay(), objectDto.getConfidence());
					if(scanService.insertScan(objectScan) == 1) {
						// Funzione che crei l' history per lo scan
						scanService.insertScanHistory(foundObjects, objectScan);
						// Aggiorniamo lo score dell'utente 
						int point = userService.updateScore(objectDto.getConfidence(), idUser);
						switch(point) {
							case 0:
								return new ResponseEntity<PostScanResponse>(new PostScanResponse(false, point), HttpStatus.OK);
							case -1:
								new ResponseEntity<PostScanResponse>(new PostScanResponse(false, point), HttpStatus.INTERNAL_SERVER_ERROR);
							default: 
								return new ResponseEntity<PostScanResponse>(new PostScanResponse(true, point), HttpStatus.OK);
						}
					}
					return new ResponseEntity<PostScanResponse>(new PostScanResponse(false, -1), HttpStatus.INTERNAL_SERVER_ERROR); // QUANDO FALLISCE LA INSERT, COSA DOVREI RESTITUIRE?
				}
			}
			return new ResponseEntity<PostScanResponse>(new PostScanResponse(false, 0), HttpStatus.OK); // Non ha trovato nessun obbiettivo 
		}
		return new ResponseEntity<PostScanResponse>(new PostScanResponse(false, -1), HttpStatus.BAD_REQUEST);
	}
	
	
}
