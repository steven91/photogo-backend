package com.certimeter.photoGo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.certimeter.photoGo.model.DailyAchivment;
import com.certimeter.photoGo.service.DailyachivmentService;

@RestController
@RequestMapping("dailyachivment")
public class DailyachivmentController {

	@Autowired
	DailyachivmentService dailyachivmentService;
	
	@GetMapping("{day}")
	public @ResponseBody ResponseEntity<DailyAchivment> getAchivment(@PathVariable int day){
		return new ResponseEntity<DailyAchivment>(dailyachivmentService.getAchivment(day), HttpStatus.OK);
	}
}
