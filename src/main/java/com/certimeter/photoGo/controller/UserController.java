package com.certimeter.photoGo.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.certimeter.photoGo.dto.UserDto;
import com.certimeter.photoGo.model.User;
import com.certimeter.photoGo.request.LoginRequest;
import com.certimeter.photoGo.request.SinginRequest;
import com.certimeter.photoGo.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@GetMapping("{idUser}")
	public @ResponseBody ResponseEntity<User> getUser(@PathVariable int idUser){
		System.out.println("RICHIESTA RICERCA UTENTE, ID = " + idUser);
		return new ResponseEntity<User>(userService.getUser(idUser), HttpStatus.OK);
	}
	
	@PostMapping("login")
	public @ResponseBody ResponseEntity<UserDto> login(@RequestBody LoginRequest loginRequest){
		System.out.println("RICHIESTA LOGIN " + loginRequest);
		if(loginRequest != null && loginRequest.isValid()) {
			User userFound = userService.login(loginRequest);
			if(userFound != null) {
				return new ResponseEntity<UserDto>(userFound.convertToUserDto(), HttpStatus.OK);
			}
			return new ResponseEntity<UserDto>(new UserDto(), HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<UserDto>(new UserDto(), HttpStatus.BAD_REQUEST);
		
	}
	
	@PostMapping("singin")
	public @ResponseBody ResponseEntity<UserDto> singin(@RequestBody SinginRequest singinRequest){
		System.out.println("RICHIESTA SINGIN " + singinRequest);
		if(singinRequest != null && singinRequest.isValid()) {
			if(userService.checkExist(singinRequest) == 1 && userService.singin(singinRequest) == 1) {
				return new ResponseEntity<UserDto>(login(new LoginRequest(singinRequest.getEmail(), singinRequest.getPassword())).getBody(), HttpStatus.OK);
			}
		}
		return new ResponseEntity<UserDto>(new UserDto(), HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("rank")
	public @ResponseBody ResponseEntity<ArrayList<UserDto>> rank(){
		System.out.println("RICHIESTA RANK");
		ArrayList<UserDto> userDto = new ArrayList<UserDto>();
		for(User user: userService.rank()) {
			userDto.add(user.convertToUserDto());
		}
		return new ResponseEntity<ArrayList<UserDto>>(userDto, HttpStatus.OK);
	}
}
