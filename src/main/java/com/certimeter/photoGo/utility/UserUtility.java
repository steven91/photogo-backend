package com.certimeter.photoGo.utility;

public class UserUtility {

	// Funzione utilizzata per calcolare il punteggio corrispondente al livello di confidenza di uno scan
	public int calculatePoint(double confidence) {
		double range = (confidence*100)/10;
		if(range <= 10 && range >= 9) { return 100; }
		else if(range <9 && range >=8) { return 80; }
		else if(range <8 && range >=6) { return 60; }
		return 0;
	}
}
