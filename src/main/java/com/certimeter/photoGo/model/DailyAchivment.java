package com.certimeter.photoGo.model;

public class DailyAchivment {

	private int day; // Valore intero che indica il giorno dell' obbiettivo
	private String targetObject;
	
	public DailyAchivment() { }
	
	public DailyAchivment(int day, String targetObject) {
		super();
		this.day = day;
		this.targetObject = targetObject;
	}



	public int getDay() { return day; }

	public void setDay(int day) {
		this.day = day;
	}

	public String getTargetObject() { return targetObject; }

	public void setTargetObject(String targetObject) {
		this.targetObject = targetObject;
	}
	
	
}
