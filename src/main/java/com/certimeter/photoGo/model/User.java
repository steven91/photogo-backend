package com.certimeter.photoGo.model;


import com.certimeter.photoGo.dto.UserDto;

public class User {
	private int idUser;
	private String username;
	private String password;
	private String email; // Utilizzata nel momento della creazione del profilo
	private int score;
	
	public User() { }

	public User(int idUSer, String username, String password, String email, int score) {
		this.idUser = idUSer;
		this.username = username;
		this.password = password;
		this.email = email;
		this.score = score;
	}
	
	public int getIdUser() { return idUser; }

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getUsername() { return username; }

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() { return password; }

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() { return email; }

	public void setEmail(String email) {
		this.email = email;
	}

	public int getScore() { return score; }

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", punteggio=" + score + "]";
	}
	
	public UserDto convertToUserDto() {
		UserDto userDto = new UserDto();
		userDto.setIdUser(idUser);
		userDto.setUsername(username);
		userDto.setEmail(email);
		userDto.setScore(score);
		return userDto;
	}
	
}
