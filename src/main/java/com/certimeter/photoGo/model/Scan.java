package com.certimeter.photoGo.model;

import java.time.OffsetDateTime;

public class Scan {
	
	private int idScan;
	private int idUser;
	private OffsetDateTime data;
	private int targetObject;
	private double confidence;

	public Scan() { }

	public Scan(int idScan, int idUser, OffsetDateTime data, int targetObject, double confidence) {
		this.idScan = idScan;
		this.idUser = idUser;
		this.data = data;
		this.targetObject = targetObject;
		this.confidence = confidence;
	}

	public int getIdScan() { return idScan; }

	public void setIdScan(int idScan) {
		this.idScan = idScan;
	}

	public int getIdUser() { return idUser; }

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public OffsetDateTime getData() { return data; }

	public void setData(OffsetDateTime data) {
		this.data = data;
	}

	public int getTargetObject() { return targetObject; }

	public void setTargetObject(int targetObject) {
		this.targetObject = targetObject;
	}

	public double getConfidence() { return confidence; }
	
	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}

	@Override
	public String toString() {
		return "Scan [idScan=" + idScan + ", idUser=" + idUser + ", data=" + data + ", targetObject=" + targetObject
				+ ",\n confidence=" + confidence + "]";
	}
	
}
