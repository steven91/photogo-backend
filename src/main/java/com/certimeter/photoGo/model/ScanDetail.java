package com.certimeter.photoGo.model;


public class ScanDetail {

	private int idScanDetail;
	private int idScanFk;
	private String objectLabel;
	private double confidence;
	
	public ScanDetail() { }

	public ScanDetail(int idScanDetail, int idScanFk, String objectLabel, double confidence) {
		this.idScanDetail = idScanDetail;
		this.idScanFk = idScanFk;
		this.objectLabel = objectLabel;
		this.confidence = confidence;
	}

	public int getIdScanDetail() { return idScanDetail; }

	public void setIdScanDetail(int idScanDetail) {
		this.idScanDetail = idScanDetail;
	}

	public int getIdScanFk() { return idScanFk; }

	public void setIdScanFk(int idScanFk) {
		this.idScanFk = idScanFk;
	}

	public String getObjectLabel() { return objectLabel; }

	public void setObjectLabel(String objectLabel) {
		this.objectLabel = objectLabel;
	}

	public double getConfidence() { return confidence; }

	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}

	@Override
	public String toString() {
		return "ScanDetail [idScanDetail=" + idScanDetail + ", idScanFk=" + idScanFk + ", objectLabel=" + objectLabel
				+ ", confidence=" + confidence + "]";
	}
	
	
}
