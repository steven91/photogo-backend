package com.certimeter.photoGo.dto;

public class UserDto {

	private int idUser;
	private String username;
	private String email;
	private int score;
		
	public UserDto() { }
	
	public UserDto(int idUser, String username, String email, int score) {
		this.idUser = idUser;
		this.username = username;
		this.email = email;
		this.score = score;
	}

	public int getIdUser() { return idUser; }

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getUsername() { return username; }

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() { return email; }

	public void setEmail(String email) {
		this.email = email;
	}

	public int getScore() { return score; }

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "UserDto [idUser=" + idUser + ", username=" + username + ", email=" + email + ", score=" + score + "]";
	}
	
	
}
