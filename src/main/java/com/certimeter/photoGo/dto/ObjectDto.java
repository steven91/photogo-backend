package com.certimeter.photoGo.dto;

// Definizione dell'oggetto analizzato dall'ML Kit
public class ObjectDto {

	private String label;
	private float confidence;
	//TODO: Aggiungere il totale delle foto disponibili (Nel caso di 10 foto al giorno )
	
	public ObjectDto() {  }

	public ObjectDto(String label, float confidence) {
		this.label = label;
		this.confidence = confidence;
	}

	public String getLabel() { return label; }

	public void setLabel(String label) {
		this.label = label;
	}

	public float getConfidence() { return confidence; }

	public void setConfidence(float confidence) {
		this.confidence = confidence;
	}

	@Override
	public String toString() {
		return "ObjectDto [label=" + label + ", confidence=" + confidence + "]";
	}

}
