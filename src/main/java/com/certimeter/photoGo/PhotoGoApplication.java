package com.certimeter.photoGo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@MapperScan("com.certimeter.photoGo.mapper")
@SpringBootApplication
public class PhotoGoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotoGoApplication.class, args);
	}

}
