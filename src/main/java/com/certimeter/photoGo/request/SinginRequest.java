package com.certimeter.photoGo.request;

import java.util.regex.Pattern;

public class SinginRequest {

	private String username;
	private String password;
	private String email;
	
	public SinginRequest() { }

	public SinginRequest(String username, String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}

	public String getUsername() { return username; }

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() { return password; }

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() { return email; }

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "SinginRequest [username=" + username + ", password=" + password + ", email=" + email + "]";
	}
	
	public boolean isValid() {
		String regexEmail = "^([a-zA-Z\\d_\\-\\.\\+%]+)@([a-zA-Z]+)\\.([a-zA-Z]{2,5})$";
		String regexPassword = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";
		Pattern pattern = Pattern.compile(regexEmail);
		Boolean result = pattern.matcher(email).matches();
		pattern = Pattern.compile(regexPassword);
		result = result && pattern.matcher(password).matches();
		return result;
	}
	
}
