package com.certimeter.photoGo.request;

import java.util.regex.Pattern;

public class LoginRequest {
	
	private String email;
	private String password;
	
	public LoginRequest() { }
	
	public LoginRequest(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public String getEmail() { return email; }

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() { return password; }

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginRequest [email=" + email + ", password=" + password + "]";
	}
	
	public boolean isValid() {
		String regexEmail = "^([a-zA-Z\\d_\\-\\.\\+%]+)@([a-zA-Z]+)\\.([a-zA-Z]{2,5})$";
		String regexPassword = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";
		Pattern pattern = Pattern.compile(regexEmail);
		Boolean result = pattern.matcher(email).matches();
		pattern = Pattern.compile(regexPassword);
		result = result && pattern.matcher(password).matches();
		return result;
	}
	

}
