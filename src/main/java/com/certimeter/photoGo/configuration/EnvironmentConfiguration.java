package com.certimeter.photoGo.configuration;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;


@Configuration
@PropertySources({
	@PropertySource("classpath:application.properties"),
	@PropertySource(value = "classpath:/${env}/database.properties", ignoreResourceNotFound = false)
})
public class EnvironmentConfiguration {
	
	@Value ("${env}")
	private String env;
	
	final  Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	public void checkEnvironmentUsed(){
		LOG.info("#######################################################");
		LOG.info("#######################################################");
		LOG.info("####################               ####################");
		LOG.info("###############      CONFIGURATION      ###############");
		LOG.info("####################               ####################");
		LOG.info("#######################################################");
		LOG.info("#######################################################");
		
		LOG.info("env => " + env);
	}
}