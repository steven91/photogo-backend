package com.certimeter.photoGo.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.certimeter.photoGo.model.Scan;
import com.certimeter.photoGo.model.ScanDetail;

@Mapper
public interface ScanMapper {

	@Select("select * from scan")
	ArrayList<Scan> getScans();
	
	@Insert("insert into scan(idUser, data, targetObject, confidence) values(#{idUser}, #{data}, #{targetObject}, #{confidence})")
	@Options(useGeneratedKeys = true, keyProperty = "idScan", keyColumn = "idScan")
	int insertScan(Scan scan);
	
	@Insert("insert into scandetail(idScanFk, objectLabel, confidence) values(#{idScanFk}, #{objectLabel}, #{confidence})")
	@Options(useGeneratedKeys = true, keyProperty = "idScanDetail", keyColumn = "idScanDetail")
	void insertScanHistory(ScanDetail scanDetail);
}
