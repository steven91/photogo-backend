package com.certimeter.photoGo.mapper;

import java.util.ArrayList;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Insert;
//import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.certimeter.photoGo.model.User;
import com.certimeter.photoGo.request.LoginRequest;
import com.certimeter.photoGo.request.SinginRequest;

@Mapper
public interface UserMapper {

	@Select("select * from user where idUser=#{idUser}")
	User getUser(@PathParam("idUser")int idUser);
	
	@Select("select * from user where email=#{email} and password=#{password}")
	User login(LoginRequest loginRequest);
	
	@Insert("insert into user(username, password, email, score) values(#{username}, #{password}, #{email}, 0)")
	@Options(useGeneratedKeys = true, keyColumn = "idUser")
	int singin(SinginRequest singinRequest);
	
	@Select("select * from user order by score desc")
	ArrayList<User> rank();
	
	@Select("select * from user where username=#{username} or email=#{email}")
	ArrayList<User> checkExist(SinginRequest singinRequest);
	
	@Update("update user set username=#{username}, password=#{password}, email=#{email}, score=#{score} where idUser = #{idUser}")
	int updateUserScore(User user);
	
}
