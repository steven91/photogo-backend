package com.certimeter.photoGo.mapper;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.certimeter.photoGo.model.DailyAchivment;

@Mapper
public interface DailyachivmentMapper {

	@Select("select * from dailyachivment where day=#{day}")
	DailyAchivment getAchivment(@PathParam("day")int day);
}
