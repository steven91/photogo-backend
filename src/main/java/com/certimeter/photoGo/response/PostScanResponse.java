package com.certimeter.photoGo.response;

public class PostScanResponse {
	
	private boolean find; 
	private int point;
	
	public PostScanResponse() {  }

	public PostScanResponse(boolean find, int point) {
		this.find = find;
		this.point = point;
	}

	public boolean getFind() { return find; }

	public void setFind(boolean find) {
		this.find = find;
	}

	public int getPoint() { return point; }

	public void setPoint(int point) {
		this.point = point;
	}

	@Override
	public String toString() {
		return "PostScanResponse [find=" + find + ", point=" + point + "]";
	}
	
	

}
