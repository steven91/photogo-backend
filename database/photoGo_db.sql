-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: photogo
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dailyachivment`
--

DROP TABLE IF EXISTS `dailyachivment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dailyachivment` (
  `day` int NOT NULL,
  `targetObject` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dailyachivment`
--

LOCK TABLES `dailyachivment` WRITE;
/*!40000 ALTER TABLE `dailyachivment` DISABLE KEYS */;
INSERT INTO `dailyachivment` VALUES (1,'Paper');
/*!40000 ALTER TABLE `dailyachivment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dailyachivmenthistory`
--

DROP TABLE IF EXISTS `dailyachivmenthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dailyachivmenthistory` (
  `idDailyAchivmentHistory` int NOT NULL AUTO_INCREMENT,
  `idDailyAchivmentFk` int DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idDailyAchivmentHistory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dailyachivmenthistory`
--

LOCK TABLES `dailyachivmenthistory` WRITE;
/*!40000 ALTER TABLE `dailyachivmenthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `dailyachivmenthistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scan`
--

DROP TABLE IF EXISTS `scan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scan` (
  `idScan` int NOT NULL AUTO_INCREMENT,
  `idUser` int DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `targetObject` int DEFAULT NULL,
  `confidence` double DEFAULT NULL,
  PRIMARY KEY (`idScan`),
  KEY `idUserFK_idx` (`idUser`),
  KEY `targetObject_idx` (`targetObject`),
  CONSTRAINT `idUserFK` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `targetObject` FOREIGN KEY (`targetObject`) REFERENCES `dailyachivment` (`day`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scan`
--

LOCK TABLES `scan` WRITE;
/*!40000 ALTER TABLE `scan` DISABLE KEYS */;
INSERT INTO `scan` VALUES (52,1,'2021-08-20 15:51:42',1,0.800000011920929),(53,15,'2021-08-23 09:22:33',1,0.8128242492675781),(54,15,'2021-08-23 09:29:43',1,0.6231134533882141),(55,15,'2021-08-23 10:49:15',1,0.7658476233482361),(56,1,'2021-08-23 12:57:38',1,0.800000011920929),(57,1,'2021-08-23 12:59:35',1,0.800000011920929),(58,1,'2021-08-23 13:04:20',1,0.800000011920929),(59,1,'2021-08-23 14:33:30',1,0.800000011920929),(60,1,'2021-08-23 14:40:08',1,0.800000011920929);
/*!40000 ALTER TABLE `scan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scandetail`
--

DROP TABLE IF EXISTS `scandetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scandetail` (
  `idScanDetail` int NOT NULL AUTO_INCREMENT,
  `idScanFk` int DEFAULT NULL,
  `objectLabel` varchar(45) DEFAULT NULL,
  `confidence` double DEFAULT NULL,
  PRIMARY KEY (`idScanDetail`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scandetail`
--

LOCK TABLES `scandetail` WRITE;
/*!40000 ALTER TABLE `scandetail` DISABLE KEYS */;
INSERT INTO `scandetail` VALUES (1,60,'prova',0.6000000238418579),(2,60,'altraProva',0.8999999761581421),(3,60,'Paper',0.800000011920929);
/*!40000 ALTER TABLE `scandetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `idUser` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `score` int DEFAULT '0',
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'prova','Prova1??','us@us.it',660),(15,'admin','Admin1??','admin@admin.it',300),(16,'ftvdhvsv','Prova1??','pro@pro.it',400);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-23 14:42:48
